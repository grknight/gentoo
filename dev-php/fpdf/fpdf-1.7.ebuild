# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Free PDF PHP Library."
HOMEPAGE="http://www.fpdf.org/"
LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

MY_PV="17"
SRC_URI="http://fpdf.de/downloads/${PN}${MY_PV}.tgz"
S="${WORKDIR}/${PN}${MY_PV}"

DOCS=( FAQ.htm fpdf.css install.txt )

src_install() {
	einstalldocs
	docinto html
	dodoc -r doc/*
#	Tutorial is broken due to bad calligra.z compression
#	dohtml-php tutorial/*
	insinto /usr/share/php/${PN}
	doins -r font fpdf.php
}
