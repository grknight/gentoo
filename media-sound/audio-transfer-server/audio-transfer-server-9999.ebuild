EAPI="7"

inherit git-r3 qmake-utils

DESCRIPTION="Audio server to receive audio/pcm stream over LAN"
HOMEPAGE="https://code.google.com/p/audio-transfer-server"

EGIT_REPO_URI="https://code.google.com/p/audio-transfer-server/"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="dev-qt/qtgui:5
	dev-qt/qtcore:5
	dev-qt/qtnetwork:5
	dev-qt/qtmultimedia:5"

DEPEND="${RDEPEND}"

src_configure() {
	eqmake5
}

src_install() {
	default
	dobin audio-transfer-server
	insinto /etc/
	doins audio-transfer-server.ini
}
