# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit multilib-minimal

DESCRIPTION="Library for correctly-rounded arbitrary precision decimal numbers"
HOMEPAGE="http://www.bytereef.org/mpdecimal/"
SRC_URI="http://www.bytereef.org/software/${PN}/releases/${P}.tar.gz"
LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs"

MULTILIB_WRAPPED_HEADERS=( usr/include/mpdecimal.h )
PATCHES=( "${FILESDIR}/2.4.2-fix-build.patch" )

HTML_DOCS=( doc/arithmetic.html doc/attributes.html doc/decimals.html
	doc/index.html doc/memory.html doc/search.html doc/_static
	doc/assign-convert.html  doc/context.html doc/functions.html
	doc/searchindex.js doc/various.html )

src_prepare() {
	default
	multilib_copy_sources
}

multilib_src_install_all() {
	if ! use static-libs ; then
		find "${D}" -name '*.a' -delete || die
	fi
	# Copies to the wrong location and includes the LICENSE
	rm -r "${D}/usr/share/doc/" || die
	einstalldocs
}
