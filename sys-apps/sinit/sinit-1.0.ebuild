# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="This is a sample skeleton ebuild file"
HOMEPAGE="http://core.suckless.org/${PN}"
SRC_URI="http://git.suckless.org/${PN}/snapshot/${P}.tar.bz2"
LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="sysv-utils"
RDEPEND="${DEPEND}
	sysv-utils? (
		!sys-apps/sysvinit
		!sys-apps/systemd-sysv-utils
		!sys-apps/systemd[sysv-utils]
	)"

src_prepare(){
	sed -i 's~/bin/rc~/etc/rc~g' "${S}/config.def.h" || die
	sed -i 's~/bin/rc~/etc/rc~g' "${S}/sinit.8" || die
	sed -i -e "s/cc/$(tc-getCC)/" -e "s/\$\(CC\)/$(tc-getLD)/" \
		-e "s/-s -static/-static/" "${S}/config.mk" || die
	default
}

src_install(){
	exeinto /sbin
	doexe sinit
	doman sinit.8
	if use sysv-utils; then
		for app in halt poweroff reboot shutdown telinit; do
			dosym "../etc/rc.shutdown" /sbin/${app}
		done
		dosym "sinit" /sbin/init
	fi
	insinto /etc
	doins "${FILESDIR}/rc.init" "${FILESDIR}/rc.shutdown"
	fperms 0744 /etc/rc.init /etc/rc.shutdown
#	newinitd "${FILESDIR}/agetty.init" agetty
#	dosym agetty /etc/init.d/agetty.tty1
}
