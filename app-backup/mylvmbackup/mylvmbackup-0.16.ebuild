# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="mylvmbackup is a tool for quickly creating backups of MySQL server's data files using LVM snapshots"
HOMEPAGE="http://lenzg.net/mylvmbackup/"
SRC_URI="http://lenzg.net/${PN}/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE=""
DEPEND=""
RDEPEND="dev-perl/Config-IniFiles
	dev-perl/DBD-mysql
	dev-perl/File-Copy-Recursive
	dev-perl/TimeDate
	>=sys-fs/lvm2-2.02.06
	virtual/mysql"

src_prepare() {
	sed -i \
		-e '/^prefix/s,/usr/local,/usr,' \
		"${S}"/Makefile || die
	sed -i 's|mycnf=/etc/my.cnf|mycnf=/etc/mysql/my.cnf|' "${S}"/mylvmbackup.conf || die
}

src_install() {
	emake install DESTDIR="${D}" mandir="/usr/share/man"
	dodoc ChangeLog README TODO
	keepdir /var/tmp/${PN}/{backup,mnt}
	fperms 0700 /var/tmp/${PN}/
}

pkg_postinst() {
	elog "This package has optional runtime features which use additional packages:"
	elog "* For SNMP reporting, install dev-perl/Net-SNMP"
	elog "* For Email reports, install dev-perl/MIME-Lite"
	elog "* For advanced, deduplicating archives, install app-backup/zbackup"
}
